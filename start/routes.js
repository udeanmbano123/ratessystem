'use strict'

/*
|--------------------------------------------------------------------------
| Routes
|--------------------------------------------------------------------------
|
| Http routes are entry points to your web application. You can create
| routes for different URL's and bind Controller actions to them.
|
| A complete guide on routing is available here.
| http://adonisjs.com/docs/4.1/routing
|
*/

/** @type {typeof import('@adonisjs/framework/src/Route/Manager')} */
const Route = use('Route')
const Database = use("Database")

Route.on('/').render('session/create')
// Must be logged in
Route.group(() => {
  // Session
Route.get("logout", "SessionController.delete");

Route.get('dashboard', ({ view }) => {
  return view.render('dashboard')
})
Route.get('/countries/index', 'CountryController.index')
Route.post('countries', 'CountryController.store')
Route.delete('countries/:id', 'CountryController.destroy')

Route.get('/paymentmethods/index', 'PaymentMethodController.index')
Route.post('paymentmethods', 'PaymentMethodController.store')
Route.delete('paymentmethods/:id', 'PaymentMethodController.destroy')

Route.get('/servicelevels/index', 'ServiceLevelController.index')
Route.post('servicelevels', 'ServiceLevelController.store')
Route.delete('servicelevels/:id', 'ServiceLevelController.destroy')

Route.get('/transfertypes/index', 'TransferTypeController.index')
Route.post('transfertypes', 'TransferTypeController.store')
Route.delete('transfertypes/:id', 'TransferTypeController.destroy')

Route.get('/promocodes/index', 'PromoCodeController.index')
Route.post('promocodes', 'PromoCodeController.store')
Route.delete('promocodes/:id', 'PromoCodeController.destroy')
                   
Route.get('/users/index', 'UserController.index')
Route.post('users', 'UserController.store')
Route.delete('users/:id', 'UserController.destroy')

Route.get('/feescharges/index', 'FeeschargeController.index')
Route.post('feescharges', 'FeeschargeController.store')
Route.delete('feescharges/:id', 'FeeschargeController.destroy')
}).middleware(["auth"]);                  
// Session
Route.get("login", "SessionController.create");
Route.post("login", "SessionController.store");
