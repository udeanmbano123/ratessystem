'use strict'
const Promocode = use('App/Models/Promocode')
const { validate } = use('Validator')
const Database = use("Database")
class PromoCodeController {
   // app/Controllers/Http/TaskController.js

// remember to reference the Task model at the top

async index ({ view }) {
    const promocodes = await Promocode.all()
  
    return view.render('promocodes.index', { promocodes: promocodes.toJSON() })
  }
  async store ({ request, response, session }) {
      // validate form input
      const validation = await validate(request.all(), {
        Code: 'required|min:3|max:255'
      })
    
      // show error messages upon validation fail
      if (validation.fails()) {
        session.withErrors(validation.messages())
                .flashAll()
    
        return response.redirect('back')
      }
    
      // persist to database
      const PromoCode = new Promocode()
      PromoCode.Code = request.input('Code')
      PromoCode.created_at =  Database.fn.now()
      PromoCode.updated_at = Database.fn.now()
      await PromoCode.save()
    
      // Fash success message to session
      session.flash({ notification: 'PromoCode added!' })
    
      return response.redirect('back')
    }
    async destroy ({ params, session, response }) {
      const PromoCode = await Promocode.find(params.id)
      await PromoCode.delete()
    
      // Fash success message to session
      session.flash({ notification: 'PromoCode deleted!' })
    
      return response.redirect('back')
    }

}

module.exports = PromoCodeController
