'use strict'
const Paymentmethod = use('App/Models/Paymentmethod')
const { validate } = use('Validator')
const Database = use("Database")
class PaymentMethodController {
   // app/Controllers/Http/TaskController.js

// remember to reference the Task model at the top

async index ({ view }) {
    const paymentmethods = await Paymentmethod.all()
  
    return view.render('paymentmethods.index', { paymentmethods: paymentmethods.toJSON() })
  }
  async store ({ request, response, session }) {
      // validate form input
      const validation = await validate(request.all(), {
        PaymentType: 'required|min:3|max:255'
      })
    
      // show error messages upon validation fail
      if (validation.fails()) {
        session.withErrors(validation.messages())
                .flashAll()
    
        return response.redirect('back')
      }
    
      // persist to database
      const PaymentMethod = new Paymentmethod()
      PaymentMethod.PaymentType = request.input('PaymentType')
      country.created_at =  Database.fn.now()
      country.updated_at = Database.fn.now()
      await PaymentMethod.save()
    
      // Fash success message to session
      session.flash({ notification: 'PaymentMethod added!' })
    
      return response.redirect('back')
    }
    async destroy ({ params, session, response }) {
      const PaymentMethod = await Paymentmethod.find(params.id)
      await PaymentMethod.delete()
    
      // Fash success message to session
      session.flash({ notification: 'PaymentMethod deleted!' })
    
      return response.redirect('back')
    }
}

module.exports = PaymentMethodController
