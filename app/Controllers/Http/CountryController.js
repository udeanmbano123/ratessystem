'use strict'
const Country = use('App/Models/Country')
const { validate } = use('Validator')
const Database = use("Database")
class CountryController {
    // app/Controllers/Http/TaskController.js

// remember to reference the Task model at the top

async index ({ view }) {
  const countries = await Country.all()

  return view.render('countries.index', { countries: countries.toJSON() })
}
async store ({ request, response, session }) {
    // validate form input
    const validation = await validate(request.all(), {
      Name: 'required|min:3|max:255',
      Currency: 'required|min:3|max:255'
    })
  
    // show error messages upon validation fail
    if (validation.fails()) {
      session.withErrors(validation.messages())
              .flashAll()
  
      return response.redirect('back')
    }
  
    // persist to database
    const country = new Country()
    country.Name = request.input('Name')
    country.Currency = request.input('Currency')
    country.created_at =  Database.fn.now()
    country.updated_at = Database.fn.now()
    await country.save()
  
    // Fash success message to session
    session.flash({ notification: 'Country added!' })
  
    return response.redirect('back')
  }
  async destroy ({ params, session, response }) {
    const country = await Country.find(params.id)
    await country.delete()
  
    // Fash success message to session
    session.flash({ notification: 'Country deleted!' })
  
    return response.redirect('back')
  }
}

module.exports = CountryController
