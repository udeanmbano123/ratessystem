'use strict'
const User= use('App/Models/User')
const { validate } = use('Validator')
const Database = use("Database")
const Hash = use("Hash")

class UserController {

       // app/Controllers/Http/TaskController.js

// remember to reference the Task model at the top

async index ({ view }) {
    const users = await User.query()
    .whereNot('username','admin')
    .fetch()
  
    return view.render('users.index', { users: users.toJSON() })
  }
  async store ({ request, response, session }) {
      // validate form input
      const validation = await validate(request.all(), {
        username: 'required|min:3|max:255',
        email: 'required|min:3|max:255',
        password: 'required|min:3|max:255'
      
      })
    
      // show error messages upon validation fail
      if (validation.fails()) {
        session.withErrors(validation.messages())
                .flashAll()
    
        return response.redirect('back')
      }
    
      // persist to database
      const user = new User()
      user.username= request.input('username')
      user.email= request.input('email')
      user.password= await Hash.make(request.input('password'))
      user.created_at =  Database.fn.now()
      user.updated_at = Database.fn.now()
      await user.save()
    
      // Fash success message to session
      session.flash({ notification: 'User added!' })
    
      return response.redirect('back')
    }
    async destroy ({ params, session, response }) {
      const User = await User.find(params.id)
      await User.delete()
    
      // Fash success message to session
      session.flash({ notification: 'User deleted!' })
    
      return response.redirect('back')
    }
}

module.exports = UserController
