'use strict'
const Servicelevel= use('App/Models/Servicelevel')
const { validate } = use('Validator')
const Database = use("Database")
class ServiceLevelController {
       // app/Controllers/Http/TaskController.js

// remember to reference the Task model at the top

async index ({ view }) {
    const servicelevels = await Servicelevel.all()
  
    return view.render('servicelevels.index', { servicelevels: servicelevels.toJSON() })
  }
  async store ({ request, response, session }) {
      // validate form input
      const validation = await validate(request.all(), {
        Name: 'required|min:3|max:255'
      })
    
      // show error messages upon validation fail
      if (validation.fails()) {
        session.withErrors(validation.messages())
                .flashAll()
    
        return response.redirect('back')
      }
    
      // persist to database
      const servicelevel = new Servicelevel()
      servicelevel.Name = request.input('Name')
      servicelevel.created_at =  Database.fn.now()
      servicelevel.updated_at = Database.fn.now()
        await servicelevel.save()
    
      // Fash success message to session
      session.flash({ notification: 'servicelevel added!' })
    
      return response.redirect('back')
    }
    async destroy ({ params, session, response }) {
      const servicelevel = await Servicelevel.find(params.id)
      await servicelevel.delete()
    
      // Fash success message to session
      session.flash({ notification: 'servicelevel deleted!' })
    
      return response.redirect('back')
    }
}

module.exports = ServiceLevelController
