'use strict'
const Transfertype= use('App/Models/Transfertype')
const { validate } = use('Validator')
const Database = use("Database")
class TransferTypeController {
       // app/Controllers/Http/TaskController.js

// remember to reference the Task model at the top

async index ({ view }) {
    const transfertypes = await Transfertype.all()
  
    return view.render('transfertypes.index', { transfertypes: transfertypes.toJSON() })
  }
  async store ({ request, response, session }) {
      // validate form input
      const validation = await validate(request.all(), {
        Type: 'required|min:3|max:255'
      })
    
      // show error messages upon validation fail
      if (validation.fails()) {
        session.withErrors(validation.messages())
                .flashAll()
    
        return response.redirect('back')
      }
    
      // persist to database
      const TransferType = new Transfertype()
      TransferType.Type = request.input('Type')
      TransferType.created_at =  Database.fn.now()
      TransferType.updated_at = Database.fn.now()
    
      await TransferType.save()
    
      // Fash success message to session
      session.flash({ notification: 'Transfer Type added!' })
    
      return response.redirect('back')
    }
    async destroy ({ params, session, response }) {
      const TransferType = await Transfertype.find(params.id)
      await TransferType.delete()
    
      // Fash success message to session
      session.flash({ notification: 'Transfer Type deleted!' })
    
      return response.redirect('back')
    }
}

module.exports = TransferTypeController
