'use strict'
const Feescharge= use('App/Models/Feescharge')
const { validate } = use('Validator')
const Database = use("Database")
class FeeschargeController {
  // app/Controllers/Http/TaskController.js

// remember to reference the Task model at the top

async index ({ view }) {
    const feescharges= await Feescharge.all()
 
    return view.render('feescharges.index', { feescharges: feescharges.toJSON() })
  }
  async store ({ request, response, session }) {
      // validate form input
      const validation = await validate(request.all(), {
        CurrencyFrom: 'required|min:3|max:255',
        CurrencyTo: 'required|min:3|max:255',
        Charges: 'required|min:3|max:255'
      
      })
    
      // show error messages upon validation fail
      if (validation.fails()) {
        session.withErrors(validation.messages())
                .flashAll()
    
        return response.redirect('back')
      }
    
      // persist to database
      const feescharge = new Feescharge()
      feescharge.CurrencyFrom= request.input('CurrencyFrom')
      feescharge.CurrencyTo= request.input('CurrencyTo')
      feescharge.Charges= request.input('Charges')
      feescharge.created_at =  Database.fn.now()
      feescharge.updated_at = Database.fn.now()
      await feescharge.save()
    
      // Fash success message to session
      session.flash({ notification: 'Feescharge added!' })
    
      return response.redirect('back')
    }
    async destroy ({ params, session, response }) {
      const feescharge = await Feescharge.find(params.id)
      await feescharge.delete()
    
      // Fash success message to session
      session.flash({ notification: 'Feescharge deleted!' })
    
      return response.redirect('back')
    }
}

module.exports = FeeschargeController
