'use strict'

/*
|--------------------------------------------------------------------------
| DatabaseSeeder
|--------------------------------------------------------------------------
|
| Make use of the Factory instance to seed database with dummy data or
| make use of Lucid models directly.
|
*/

/** @type {import('@adonisjs/lucid/src/Factory')} */
const Factory = use('Factory')
const Database = use("Database")
const Hash = use("Hash")
class DatabaseSeeder {
  async run () {
      await Database.insert({
      username: "admin",
      email: "udeanmbano@gmail.com",
      password:await Hash.make("123456"),
      }).into("users")

  }
}

module.exports = DatabaseSeeder
