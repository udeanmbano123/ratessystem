'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class RedirectsSchema extends Schema {
  up () {
    this.create('redirects', (table) => {
      table.increments()
      table.string("from")
      table.string("to")
      table.timestamps()
    })
    this.create('countries', (table) => {
      table.increments()
      table.string('Name')
      table.string('Currency')
      table.timestamps()    
    })
    this.create('paymentmethods', (table) => {
      table.increments()
      table.string('PaymentType')
      table.timestamps()    
    })
    this.create('promocodes', (table) => {
      table.increments()
      table.string('Code')
      table.timestamps()    
    })
    this.create('servicelevels', (table) => {
      table.increments()
      table.string('Name')
      table.timestamps()    
    })
    this.create('transfertypes', (table) => {
      table.increments()
      table.string('Type')
      table.timestamps()    
    })
    this.create('feescharges', (table) => {
      table.increments()
      table.string('CurrencyFrom')
      table.string('CurrencyTo')
      table.decimal('Charges')
      table.timestamps()    
    })
  }

  down () {
    this.drop('redirects')
    this.drop('countries')
    this.drop('paymentmethods')
    this.drop('promocodes')
    this.drop('servicelevels')
    this.drop('transfertypes') 
    this.drop('feescharges')
  }
}

module.exports = RedirectsSchema
